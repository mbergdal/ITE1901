package com.headcrest.Faking;

import org.junit.Test;

import static org.junit.Assert.*;

public class CarTests {

    @Test
    public void StartCar() {
        Car car = new Car();
        car.start();
        assertTrue(car.isStarted());
    }

    @Test
    public void StartCar_lowFuel_CarNotStared() {
        Car car = new Car();
        car.start();
        assertFalse(car.isStarted());
    }
}