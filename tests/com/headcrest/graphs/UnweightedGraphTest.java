package com.headcrest.graphs;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class UnweightedGraphTest {
    City[] vertices = {new City(0, "Seattle", 75, 50),
            new City(1, "San Francisco", 50, 210),
            new City(2, "Los Angeles", 75, 275),
            new City(3, "New York", 75, 275)
    };

    int[][] edges = {
            {0, 1}, {0, 2},
            {1, 0}, {1, 2}, {1, 3},
            {2, 0}, {2, 1},
            {3, 1}
    };

    Graph<City> graph = new UnweightedGraph<>(vertices, edges);

    @Test
    public void dfs(){
        AbstractGraph<City>.Tree dfs = graph.dfs(0);

        int numberOfVerticesFound = dfs.getNumberOfVerticesFound();

        assertEquals(4, numberOfVerticesFound);
        List<Integer> searchOrder = dfs.getSearchOrder();
        assertEquals(0, (int)searchOrder.get(0));
        assertEquals(1, (int)searchOrder.get(1));
        assertEquals(2, (int)searchOrder.get(2));
        assertEquals(3, (int)searchOrder.get(3));

    }

    @Test
    public void bfs(){

        AbstractGraph<City>.Tree bfs = graph.bfs(3);

        int numberOfVerticesFound = bfs.getNumberOfVerticesFound();

        assertEquals(4, numberOfVerticesFound);
        List<Integer> searchOrder = bfs.getSearchOrder();
        assertEquals(3, (int)searchOrder.get(0));
        assertEquals(1, (int)searchOrder.get(1));
        assertEquals(0, (int)searchOrder.get(2));
        assertEquals(2, (int)searchOrder.get(3));

    }

    @Test
    public void bfs_bigger(){
        String[] vertices = {"Seattle", "San Francisco", "Los Angeles",
                "Denver", "Kansas City", "Chicago", "Boston", "New York",
                "Atlanta", "Miami", "Dallas", "Houston"};

        int[][] edges = {
                {0, 1}, {0, 3}, {0, 5}, //0-Seattle
                {1, 0}, {1, 2}, {1, 3}, //1-San Francisco
                {2, 1}, {2, 3}, {2, 4}, {2, 10}, //2-Los Angeles
                {3, 0}, {3, 1}, {3, 2}, {3, 4}, {3, 5}, //3-Denver
                {4, 2}, {4, 3}, {4, 5}, {4, 7}, {4, 8}, {4, 10}, //4-Kansas City
                {5, 0}, {5, 3}, {5, 4}, {5, 6}, {5, 7}, //5-Chicago
                {6, 5}, {6, 7}, //6-Boston
                {7, 4}, {7, 5}, {7, 6}, {7, 8}, //7-New York
                {8, 4}, {8, 7}, {8, 9}, {8, 10}, {8, 11}, //8-Atlanta
                {9, 8}, {9, 11}, //9-Miami
                {10, 2}, {10, 4}, {10, 8}, {10, 11}, //10-Dallas
                {11, 8}, {11, 9}, {11, 10} //11-Houston
        };

        Graph<String> graph = new UnweightedGraph<>(vertices, edges);
        AbstractGraph<String>.Tree bfs =
                graph.bfs(graph.getIndex("Chicago"));


        assertEquals(12, bfs.getNumberOfVerticesFound());
        List<Integer> searchOrder = bfs.getSearchOrder();
        assertEquals("Chicago", vertices[searchOrder.get(0)]);
        assertEquals("Seattle", vertices[searchOrder.get(1)]);
        assertEquals("Denver", vertices[searchOrder.get(2)]);
        assertEquals("Kansas City", vertices[searchOrder.get(3)]);
        assertEquals("Boston", vertices[searchOrder.get(4)]);
        assertEquals("New York", vertices[searchOrder.get(5)]);
        assertEquals("San Francisco", vertices[searchOrder.get(6)]);
        assertEquals("Los Angeles", vertices[searchOrder.get(7)]);
        assertEquals("Atlanta", vertices[searchOrder.get(8)]);
        assertEquals("Dallas", vertices[searchOrder.get(9)]);
        assertEquals("Miami", vertices[searchOrder.get(10)]);
        assertEquals("Houston", vertices[searchOrder.get(11)]);

        assertEquals("Atlanta", graph.getVertex(bfs.getParent(11)));
    }
}