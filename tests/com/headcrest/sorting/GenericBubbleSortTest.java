package com.headcrest.sorting;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static org.junit.Assert.*;

public class GenericBubbleSortTest {

    private Object ArrayList;

    @Test
    public void bubbleSort(){
        Integer[] unsorted = {2,4,5,6,1,3,7,8};
        GenericBubbleSort.bubbleSort(unsorted);
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8}, unsorted);
    }

    @Test
    public void bubbleSortWithComarator(){
        Integer[] unsorted = {2,4,5,6,1,3,7,8};
        GenericBubbleSort.bubbleSort(unsorted, Collections.reverseOrder());
        assertArrayEquals(new Integer[]{8, 7, 6, 5, 4, 3, 2, 1}, unsorted);
    }
}