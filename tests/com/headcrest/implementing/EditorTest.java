package com.headcrest.implementing;

import org.junit.Test;

import static org.junit.Assert.*;

public class EditorTest {

    @Test
    public void addTwoWords_DisplaysWords() {
        Editor editor = new Editor();
        editor.addWord("Foo");
        editor.addWord("Bar");
        String words = editor.getWords();
        assertEquals("FooBar", words);
    }

    @Test
    public void addTwoWords_undo_dipsplayOneWord() {
        Editor editor = new Editor();
        editor.addWord("Foo");
        editor.addWord("Bar");
        editor.undo();
        String words = editor.getWords();
        assertEquals("Foo", words);
    }

}