package com.headcrest.implementing;

import org.junit.Test;

import static org.junit.Assert.*;

public class WordReverserTest {

    @Test
    public void reverseWord() {
        WordReverser w = new WordReverser();
        String reversed = w.reverse("Hello");
        assertTrue("olleH".equals(reversed));
    }
}