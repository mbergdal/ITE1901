package com.headcrest.generics.house;

import org.junit.Test;

import static org.junit.Assert.*;

public class GenericHouseTests {

    @Test
    public void addPersonToHouse() {
        GenericHouse<Person> house = new GenericHouse<Person>();
        house.add(new Person());
        assertCorrectInhabitant(house);
    }

    @Test
    public void addDogToHouse() {
        GenericHouse<Dog> house = new GenericHouse<>();
        house.add(new Dog());
        assertCorrectInhabitant(house);
    }

    @Test
    public void addDogToPersonHouse() {
        GenericHouse<Person> house = new GenericHouse<>();
        //house.add(new Dog());
    }

    private <T> void assertCorrectInhabitant(GenericHouse<T> house){
        T p = house.remove();
        assertNotNull(p);
    }
}