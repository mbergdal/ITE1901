package com.headcrest.generics.house;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class HouseTests {

    @Test
    public void addPersonToHouse() {
        House house = new House();
        house.add(new Person());
        house.add(new Person());
        assertCorrectInhabitant(house);
        assertEquals(1, house.getNumberOfInhabitants());
    }

    @Test
    @Ignore
    public void addDogToHouse() {
        House house = new House();
        house.add(new Dog());
        assertCorrectInhabitant(house);
    }

    private void assertCorrectInhabitant(House house){
        Person p = (Person)house.remove();
        assertNotNull(p);
    }
}