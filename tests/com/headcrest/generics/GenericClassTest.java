package com.headcrest.generics;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class GenericClassTest {

    @Test
    public void test1(){
        ArrayList things = new ArrayList(Arrays.asList("aString", new Integer(1), 2));
        GenericClass.print(things);
    }
}