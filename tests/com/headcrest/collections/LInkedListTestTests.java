package com.headcrest.collections;

import org.junit.Test;
import static org.junit.Assert.*;

public class LInkedListTestTests {

    @Test
    public void removeBerlin_BerlinNotInList() {
        LinkedListTest l = new LinkedListTest();
        assertTrue(l.getCities().contains("Berlin"));
        l.getCities().remove("Berlin");
        assertFalse(l.getCities().contains("Berlin"));
    }

}