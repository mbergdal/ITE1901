package com.headcrest.collections;

import org.junit.Test;

import static org.junit.Assert.*;

public class EvaluateExpressionTest {
    @Test
    public void evaluateExpression() throws Exception {
        int result = EvaluateExpression.evaluateExpression("(1+2)*4-3");
        assertEquals(9, result);
    }

}