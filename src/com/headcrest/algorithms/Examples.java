package com.headcrest.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mbe on 21/09/15.
 */
public class Examples {

    public static void main(String[] args) {
        //runExample1();
        //runExample2();
        //runExample3();
           //runExample4();
        runExample5();

    }


    public static void Example1(long n){
        long startTime = System.currentTimeMillis();

        long k = 0;
        for (int i = 1; i <= n ; i++) {
            k = k + 5;
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time for n = " + n + " was " + (endTime - startTime) + " ms");
    }








    private static void Example2(long n){
        long startTime = System.currentTimeMillis();

        long k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                k = k + i + j;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time for n = " + n + " was " + (endTime - startTime) + " ms");
    }



    private static void Example3(long n){
        long startTime = System.currentTimeMillis();

        long k = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= 20; j++) {
                k = k + i + j;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time for n = " + n + " was " + (endTime - startTime) + " ms");
    }

    private static void Example4(long n){
        long startTime = System.currentTimeMillis();
        long k = 0;

        for (int i = 1; i <= 10; i++) {
            k = k + 4;
        }

        for (int i = 1; i <= n ; i++) {
            for (int j = 1; j <= 20; j++) {
                k = k + i + j;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time for n = " + n + " was " + (endTime - startTime) + " ms");
    }

    private static void Example5(List<Integer> list){
        long startTime = System.currentTimeMillis();

        if (list.contains(2)){
            System.out.print("success  --- ");
        }else{
            for (Integer integer : list) {
                System.out.print(integer);
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Execution time for n = " + list.size() + " was " + (endTime - startTime) + " ms");
    }

    private static void runExample1(){
        Example1(1000000);
        Example1(10000000);
        Example1(100000000);
        Example1(1000000000);
    }

    private static void runExample2(){
        Example2(100);
        Example2(1000);
        Example2(10000);
        Example2(100000);
    }

    private static void runExample3(){
        Example3(10000);
        Example3(100000);
        Example3(1000000);
        Example3(10000000);
    }


    private static void runExample4(){
        Example4(1000000);
        Example4(10000000);
        Example4(100000000);
        Example4(1000000000);
    }

    private static void runExample5(){
        Example5(createList(100000));
        Example5(createList(1000000));
        Example5(createList(10000000));
        Example5(createList(100000000));
    }

    private static List<Integer> createList(int n){
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            integers.add(i);
        }

        Collections.shuffle(integers);
        return integers;
    }

}
