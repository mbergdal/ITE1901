package com.headcrest.algorithms;

public class TestQueens {

    //http://eightqueen.becher-sundstroem.de/
    private static final int SIZE = 30;

    public static void main(String[] args) {
        System.out.println("Recursive (Size = " + SIZE + ")");
        runRecursive();
        System.out.println("Backtrack (Size = " + SIZE + ")");
        runBacktrack();
    }

    private static void runRecursive(){
        QueensRecursive q = new QueensRecursive(SIZE);
        long start = System.currentTimeMillis();
        q.search(0);
        long stop = System.currentTimeMillis();
        System.out.println(stop-start + " ms");
    }

    private static void runBacktrack(){
        EightQueens q = new EightQueens(SIZE);
        long start = System.currentTimeMillis();
        q.search();
        long stop = System.currentTimeMillis();
        System.out.println(stop-start + " ms");
    }
}
