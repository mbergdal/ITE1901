package com.headcrest.Faking;

public class Car {
    private Engine engine;
    private Driver driver;
    private boolean isStarted;

    public Car(){
        engine = new PetrolEngine(100);
        driver = new Driver("123456789");
    }

    public void start(){
        if (engine.start() && driver.hasValidLicence()){
            isStarted = true;
        }
    }

    public boolean isStarted() {
        return isStarted;
    }
}
