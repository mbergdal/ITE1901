package com.headcrest.Faking;

public abstract class Engine {
    int fuelPercentage;

    public abstract boolean start();

    public void setFuelPersentage(int fuelPercentage){
        this.fuelPercentage = fuelPercentage;
    }

    public int getFuelPersentage(){
        return this.fuelPercentage;
    }
}
