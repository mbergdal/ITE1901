package com.headcrest.Faking;

public class Driver {
    private String licenceNbr;

    public Driver(String licenceNbr){
        this.licenceNbr = licenceNbr;
    }

    public String getLicenceNbr() {
        return licenceNbr;
    }

    public boolean hasValidLicence() {
        return licenceNbr.length() > 5;
    }
}
