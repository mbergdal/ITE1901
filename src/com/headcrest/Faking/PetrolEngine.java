package com.headcrest.Faking;

public class PetrolEngine extends Engine {

    public PetrolEngine(int initialFuelLevel) {
        super.setFuelPersentage(initialFuelLevel);
    }

    @Override
    public boolean start() {
        if (fuelPercentage > 10)
            return true;

        return false;
    }
}
