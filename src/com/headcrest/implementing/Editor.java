package com.headcrest.implementing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Editor {
    GenericStack<String> stack = new GenericStack<>();

    public static void main(String[] args) {
        Editor editor = new Editor();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a words. Enter after each word 'save' to print, 'undo' to undo last word. 'exit' to exit");
        System.out.println("Enter a word: ");
        while(input.hasNext()) {
            String word = input.nextLine();
            if (word.equals("undo")){
                if(editor.undo()){
                    System.out.println("Removed last word");
                }
            }else if(word.equals("save")){
                System.out.println(editor.getWords());
            }else if(word.equals("exit")){
                System.exit(0);
            }
            else {
                editor.addWord(word);
            }
            System.out.println("Enter a word: ");
        }
    }


    public void addWord(String word) {
        stack.push(word);
    }

    public String getWords() {
        ArrayList<String> returnStrings = new ArrayList<>();
        while(stack.getSize() != 0){
            returnStrings.add(stack.pop());
        }

        String returnString = "";
        returnStrings.sort(Collections.reverseOrder());
        for(String s : returnStrings){
            returnString += s;
        }

        return returnString;
    }

    public boolean undo() {
        if (stack.getSize() > 0) {
            stack.pop();
            return true;
        }

        return false;
    }
}
