package com.headcrest.implementing;

public class WordReverser {
    public String reverse(String original) {
        GenericStack<Character> stack = new GenericStack<>();
        for (Character c: original.toCharArray()) {
            stack.push(c);
        }

        StringBuilder outputString = new StringBuilder();
        while(!stack.isEmpty()){
            outputString.append(stack.pop());
        }

        return outputString.toString();
    }
}
