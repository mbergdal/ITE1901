package com.headcrest.hashing;

import java.util.List;
import java.util.Set;

public class MyHashTable<TKey, TValue> implements IMyHashTable<TKey, TValue> {

    private final double fillFactor = 0.75;
    private int maxItemsAtCurrentSize;
    private int count;
    private HashTableArray<TKey, TValue> array;

    public MyHashTable() {
        this(1000);
    }

    public MyHashTable(int initialCapacity) throws IllegalArgumentException {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("initialCapacity");
        }

        array = new HashTableArray<TKey, TValue>(initialCapacity);
        maxItemsAtCurrentSize = (int) (initialCapacity * fillFactor) + 1;
    }

    @Override
    public void add(TKey key, TValue value) {
        if (count >= maxItemsAtCurrentSize) {
            HashTableArray<TKey, TValue> largerArray = new HashTableArray<TKey, TValue>(array.capacity() * 2);
            for (HashTableNodePair<TKey, TValue> node : array.items()) {
                largerArray.add(node.key, node.value);
            }

            array = largerArray;
            maxItemsAtCurrentSize = (int) (array.capacity() * fillFactor) + 1;
        }

        array.add(key, value);
        count++;
    }

    @Override
    public boolean remove(TKey key) {
        boolean removed = array.remove(key);
        if (removed) {
            count--;
        }

        return removed;
    }

    @Override
    public void update(TKey key, TValue value) {
        array.update(key, value);
    }

    @Override
    public TValue get(TKey key) {
        return array.get(key);
    }

    @Override
    public boolean containsKey(TKey key) {
        for (TKey foundKey : array.keys()) {
            if (key.equals(foundKey))
                return true;
        }

        return false;
    }

    @Override
    public boolean containsValue(TValue value) {
        for (TValue foundValue : array.values()) {
            if (value.equals(foundValue))
                return true;
        }

        return false;
    }

    @Override
    public Set<TKey> keys() {
        return array.keys();
    }

    @Override
    public List<TValue> values() {
        return array.values();
    }

    @Override
    public void clear() {
        array.clear();
        count = 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }
}

