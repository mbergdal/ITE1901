package com.headcrest.hashing;

import java.util.List;
import java.util.Set;

public interface IMyHashTable<TKey, TValue> {
    void add(TKey key, TValue value);

    boolean remove(TKey key);

    void update(TKey key, TValue value);

    TValue get(TKey key);

    boolean containsKey(TKey key);

    boolean containsValue(TValue value);

    Set<TKey> keys();

    List<TValue> values();

    void clear();

    int size();

    boolean isEmpty();
}
