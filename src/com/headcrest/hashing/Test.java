package com.headcrest.hashing;


import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        IMyHashTable<String, Person> hashTable = new MyHashTable<>(10);
        Person p1 = new Person("Bob");
        hashTable.add(p1.getFirstName(), p1);
        Object o = new Object();

        System.out.println(hashTable.get(p1.getFirstName()));
    }
}

class Person{

    private final String firstName;

    public Person(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString(){
        return firstName;
    }
}
