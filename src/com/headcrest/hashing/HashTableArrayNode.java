package com.headcrest.hashing;

import java.util.*;

public class HashTableArrayNode<TKey, TValue>
{
    private LinkedList<HashTableNodePair<TKey, TValue>> items;

    public void add(TKey key, TValue value) {
        if (items == null) {
            items = new LinkedList<>();
        } else {
            for (HashTableNodePair<TKey, TValue> pair : items) {
                if (pair.key.equals(key)) {
                    throw new IllegalArgumentException("The collection already contains the key");
                }
            }
        }

        items.addFirst(new HashTableNodePair<>(key, value));
    }

    void update(TKey key, TValue value) {
        boolean updated = false;

        if (items != null) {
            for (HashTableNodePair<TKey, TValue> pair : items) {
                if (pair.key.equals(key)) {
                    pair.value = value;
                    updated = true;
                    break;
                }
            }
        }

        if (!updated) {
            throw new IllegalArgumentException("The collection does not contain the key");
        }
    }

    public TValue get(TKey key)
    {
        if (items != null) {
            for (HashTableNodePair<TKey, TValue> pair : items) {
                if (pair.key.equals(key)) {
                    return pair.value;
                }
            }
        }

        return null;
    }

    public boolean remove(TKey key)
    {
        boolean removed = false;
        if (items != null) {
            for (HashTableNodePair<TKey, TValue> current : items) {

                if (current.key.equals(key)) {
                    items.remove(current);
                    removed = true;
                    break;
                }
            }
        }

        return removed;
    }

    public void clear()
    {
        if (items != null) {
            items.clear();
        }
    }


    List<TValue> values()
    {
        List<TValue> values = new ArrayList<TValue>();

        if (items != null) {
            for (HashTableNodePair<TKey, TValue> node : items) {
                values.add(node.value);
            }
        }

        return values;
    }

    Set<TKey> keys()
    {
        Set<TKey> keys = new HashSet<>();
        if (items != null) {
            for (HashTableNodePair<TKey, TValue> node : items) {
                keys.add(node.key);
            }
        }

        return keys;
    }

    List<HashTableNodePair<TKey, TValue>> items() {
        List<HashTableNodePair<TKey, TValue>> returnList = new ArrayList<>();
        if (items != null) {
            for (HashTableNodePair<TKey, TValue> node : items) {
                returnList.add(node);
            }
        }

        return returnList;
    }
}