package com.headcrest.hashing;

public class HashTableNodePair<TKey, TValue> {
    public TKey key;
    public TValue value;

    public HashTableNodePair(TKey key, TValue value) {
        this.key = key;
        this.value = value;
    }
}
