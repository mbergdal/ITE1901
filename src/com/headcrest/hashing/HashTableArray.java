package com.headcrest.hashing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashTableArray<TKey, TValue> {
    private HashTableArrayNode<TKey, TValue>[] array;

    HashTableArray(int capacity) {
        array = new HashTableArrayNode[capacity];
        for (int i = 0; i < capacity; i++) {
            array[i] = new HashTableArrayNode<TKey, TValue>();
        }
    }

    public void add(TKey key, TValue value) {
        array[getIndex(key)].add(key, value);
    }

    public void update(TKey key, TValue value) {
        array[getIndex(key)].update(key, value);
    }

    public TValue get(TKey key) {
        return array[getIndex(key)].get(key);
    }

    public boolean remove(TKey key) {
        return array[getIndex(key)].remove(key);
    }

    int capacity() {
        return array.length;
    }

    public void clear() {
        for (HashTableArrayNode<TKey, TValue> node : array) {
            node.clear();
        }
    }

    List<TValue> values() {
        List<TValue> values = new ArrayList<>();
        for (HashTableArrayNode<TKey, TValue> node : array) {
            for (TValue value : node.values()) {
                values.add(value);
            }
        }
        return values;

    }

    Set<TKey> keys() {
        Set<TKey> keys = new HashSet<>();
        for (HashTableArrayNode<TKey, TValue> node : array) {
            for (TKey key : node.keys()) {
                keys.add(key);
            }
        }

        return keys;
    }

    List<HashTableNodePair<TKey, TValue>> items() {
        List<HashTableNodePair<TKey, TValue>> returnList = new ArrayList<>();

        for (HashTableArrayNode<TKey, TValue> node : array) {
            for (HashTableNodePair<TKey, TValue> pair : node.items()) {
                returnList.add(pair);
            }
        }

        return returnList;
    }

    private int getIndex(TKey key) {
        return Math.abs(key.hashCode() % capacity());
    }
}
