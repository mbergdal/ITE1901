package com.headcrest.collections;

import java.util.Arrays;
import java.util.LinkedList;

public class LinkedListTest {

    private LinkedList<String> cities;

    public LinkedListTest() {
        this.cities = new LinkedList<>(Arrays.asList("London", "Paris", "New York", "Berlin"));
    }

    public LinkedList<String> getCities() {
        return cities;
    }
}
