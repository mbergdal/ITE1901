package com.headcrest.collections;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;

public class Exercise20_04 {

  // Each row in points represents a point
  private double[][] points;

  /** Define a class for a point with x- and y- coordinates */
  static class Point implements Comparable<Point> {
    double x;
    double y;

    Point(double x, double y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public int compareTo(Point p2) {
      // Implement this
      throw new NotImplementedException();
    }
    
    @Override
    public String toString() {
      return "(" + x + ", " + y + ")";
    }
  }

  /**
   * A comparator for comparing points on their y-coordinates. If y-coordinates
   * are the same, compare their x-coordinator.
   */
  static class CompareY implements java.util.Comparator<Point> {
    public int compare(Point p1, Point p2) {
      // Implement this
      throw new NotImplementedException();
    }
  }
}
