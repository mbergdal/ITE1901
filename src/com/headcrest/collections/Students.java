package com.headcrest.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Students {

    Map<Student, Integer> students = new TreeMap<>();

    public void add(Student student) {
        students.put(student, student.getAge());
    }


    public int getNumberOfStudents() {
        return students.size();
    }
}
