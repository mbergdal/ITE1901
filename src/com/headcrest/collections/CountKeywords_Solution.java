package com.headcrest.collections;

import java.util.*;
import java.io.*;

public class CountKeywords_Solution {
  public static void main(String[] args) throws Exception {  
    Scanner input = new Scanner(System.in);
    System.out.print("Enter a Java source file: ");
    String filename = input.nextLine();

    File file = new File(filename);
    if (file.exists()) {
      System.out.println("The number of keywords in " + filename 
        + " is " + countKeywords(file));
    }
    else {
      System.out.println("File " + filename + " does not exist");
    }    
  }

  public static int countKeywords(File file) throws Exception {
    Set<String> keywordSet =
      new HashSet<>(Arrays.asList(Keywords.KEYWORDS_STRING));
    int count = 0;    

    Scanner input = new Scanner(file);

    while (input.hasNext()) {
      String word = input.next();
      if (keywordSet.contains(word)) 
        count++;
    }

    return count;
  }
}
