package com.headcrest.collections;

import java.util.List;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Exercise20_02 extends Application {
  private List<Integer> list = null; //Choose a list here

  private TextField tfNumber = new TextField();
  private TextArea taNumbers = new TextArea();
  private Button btSort = new Button("Sort");
  private Button btShuffle = new Button("Shuffle");
  private Button btReverse = new Button("Reverse");

  @Override
  public void start(Stage primaryStage) {
    HBox hBox = new HBox(10);
    hBox.getChildren().addAll(new Label("Enter a number: "), tfNumber);
    hBox.setAlignment(Pos.CENTER);

    BorderPane borderPane = new BorderPane();
    borderPane.setTop(hBox);
    borderPane.setCenter(new ScrollPane(taNumbers));

    HBox hBoxForButtons = new HBox(10);
    hBoxForButtons.getChildren().addAll(btSort, btShuffle, btReverse);
    hBoxForButtons.setAlignment(Pos.CENTER);
    borderPane.setBottom(hBoxForButtons);

    Scene scene = new Scene(borderPane, 400, 210);
    primaryStage.setTitle("Exercise20_02"); // Set the stage title
    primaryStage.setScene(scene); // Place the scene in the stage
    primaryStage.show(); // Display the stage

    tfNumber.setOnAction(e -> {
      if (!list.contains(new Integer(tfNumber.getText()))) {
        taNumbers.appendText(tfNumber.getText() + " ");
        list.add(new Integer(tfNumber.getText()));
      }
    });

    // Implement here. Hint: See Collections-documentation
    btSort.setOnAction(e -> {
      // Implement this
      display();
    });

    btShuffle.setOnAction(e -> {
      // Implement this
      display();
    });

    btReverse.setOnAction(e -> {
      // Implement this
      display();
    });
  }

  private void display() {
    taNumbers.setText(null);
    for (Integer i: list) {
      taNumbers.appendText(i + " ");
    }
  }
}
