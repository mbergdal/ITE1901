List (ArrayList, LinkedList), Stack, Queue, Deque (Double ended queue), PriorityQueue, Set, Map

All assignments should be done with unit tests

1.  [Linked List]
    Make a class that contains a linked List with the words "London", "Paris, "New York", "Berlin";
    - Remove "Berlin" from the list. Assert that Berlin is not in the list
    - Add "Oslo" to the list. Assert that Oslo is in the list
    - Sort the list in decreasing order. Assert that the cities are in the correct order

2.  [Stack]
    Make a class that can take a text file, and push all the words to a stack
    - Make the class able to return the number of words in the file
    - Make the class able to give you the contents of the file in reverse order

4.  [Linked List]
    - 20.2
    - Use the class Exercise20_02 as a starting point

5.  [Comparable/Comparator]
    - 20.4
    - Use the class Exercise20_04 as a starting point

6.  [Stack]
    - 20.14
    - Also part of Oblig 1

7.  [Set]
    - Make a class that can keep numbers sorted using a TreeSet
    - It should have methods for showing the first and last numbers
    - It should hava a method for returning all the numbers greater than a given number
        (hint: See documentation for the method "headSet".

8.  [Set]
    Count Keywords
    - Make a class that can take a java file and count the java-keywords in it.
    - Use the class CountKeywords as a starting point. The java keywords can be found in the file Keywords.java

9.  [Set]
    Make a class that contains two sets with strings
    - Make the class return the words that are the same in the two sets
    - Make the class return the words that are distinct in the two sets
    - Make the class return the words that are shared in the two sets

10. [Map]
    Make a class that can add student objects (with a first name, last name and age) and store the students in
    a HashMap with the Student as key and age as value.
    - The class should be able to return the number of students stored
    - Make the class store the students sorted on last name. (Hint: switch to a TreeMap
    - Make the class return a ArrayList of the students sorted on last name
    - Male the class return a students age given the last name

11. [Map]
    Count Words
    - Make class that takes a textfile and counts the number of times the words appear
    - Use the class CountOccurrenceOfWords as a starting point.


