package com.headcrest.collections;

public class Circle {


    private final int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public double getArea(){
        return radius * radius * Math.PI;
    }
}
