package com.headcrest.generics.house;

import java.util.Stack;

public class GenericHouse<T> {
    Stack<T> inhabitants = new Stack();

    public void add(T inhabitant){
        inhabitants.push(inhabitant);
    }

    public T remove(){
        if (inhabitants.isEmpty()){
            return null;
        }

        return inhabitants.pop();

    }

    public int getNumberOfInhabitants() {
        return inhabitants.size();
    }
}
