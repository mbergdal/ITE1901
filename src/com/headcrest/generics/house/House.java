package com.headcrest.generics.house;

import java.util.Stack;

public class House {
    Stack inhabitants = new Stack();

    public void add(Object p){
        inhabitants.push(p);
    }

    public Object remove(){
        if (inhabitants.isEmpty()){
            return null;
        }

        return inhabitants.pop();
    }

    public int getNumberOfInhabitants() {
        return inhabitants.size();
    }
}
