package com.headcrest.bst.Oblig2;

import java.util.AbstractSequentialList;
import java.util.ListIterator;

/*
Minimum metoder som må implementeres ifm dobbeltlenka liste oppgaven:

public TwoWayLinkedList(E[] elements)
public void addFirst(E e)
public void addLast(E e)
public void add(int index, E e)
public E remove(int index)

        I iteratorklassen må du som minimum implementere metodene (studer java.util.ListIterator klassen)
public boolean hasPrevious()
public E previous()
*/

public class TwoWayLinkedList<E> extends AbstractSequentialList<E> {
	
	public static void main(String[] args) {
	    TwoWayLinkedList<Integer> list = new TwoWayLinkedList<Integer>();
	    list.add(1);
	    list.add(2);
	    list.add(3);
	    list.add(4);

	    ListIterator<Integer> iterator = list.listIterator();
	    iterator.next();
	    iterator.next();
	    iterator.next();

	    // System.out.print(iterator.next() + " ");

	    System.out.println();
	    while (iterator.hasPrevious())
	      System.out.print(iterator.previous() + " ");
	    
	    Integer[] arrOfInt = {5,6,7};
	    TwoWayLinkedList<Integer> listFromArray = new TwoWayLinkedList<Integer>(arrOfInt);
	    System.out.println();
	    ListIterator<Integer> iterator2 = listFromArray.listIterator();
	    while (iterator2.hasNext())
	      System.out.print(iterator2.next() + " ");
	    
	}
	
	private Node<E> head;
	private Node<E> tail;
	private int size;
	  
	/** Create a default list */
	  public TwoWayLinkedList() {
	  }

	  /** Create a list from an array of objects */
	  public TwoWayLinkedList(E[] elements) {
	    for (E e : elements)
	    	addLast(e);
	  }

	  /** Return the head element in the list */
	  public E getFirst() {
	    if (size == 0) {
	      return null;
	    }
	    else {
	      return head.element;
	    }
	  }

	  /** Return the last element in the list */
	  public E getLast() {
	    if (size == 0) {
	      return null;
	    }
	    else {
	      return tail.element;
	    }
	  }

	  /** Add an element to the beginning of the list */
	  public void addFirst(E e) {
	    Node<E> newNode = new Node<E>(e); // Create a new node
	    newNode.next = head; // link the new node with the head
	    head = newNode; // head points to the new node
	    size++; // Increase list size
	    
	    if (tail == null) // the new node is the only node in list
	      tail = head;
	    
	    if (head != tail)
	        head.next.prev = head; // For a two-way linked list
	  }

	  /** Add an element to the end of the list */
	  public void addLast(E e) {
	    Node<E> newNode = new Node<E>(e); // Create a new for element e
	    
	    Node<E> temp = tail; // For a two-way linked list
	    
	    if (tail == null) {
	      head = tail = newNode; // The new node is the only node in list
	    }
	    else {
	      tail.next = newNode; // Link the new with the last node
	      tail = tail.next; // tail now points to the last node
	    }

	    size++; // Increase size
	    tail.prev = temp; // For a two-way linked list
	  }


	  @Override /** Add a new element at the specified index 
	   * in this list. The index of the head element is 0 */
	  public void add(int index, E e) {
		  
		  if (index == 0) {
	      addFirst(e);
	    }
	    else if (index >= size) {
	      addLast(e);
	    }
	    else {
	      Node<E> current = head;
	      for (int i = 1; i < index; i++) {
	        current = current.next;
	      }
	      Node<E> temp = current.next;
	      current.next = new Node<E>(e);
	      (current.next).next = temp;
	      size++;
	      // For a two-way linked list
	      temp.prev = current.next;
	      current.next.prev = current;
	    }
	  }

	  /** Remove the head node and
	   *  return the object that is contained in the removed node. */
	  public E removeFirst() {
	    if (size == 0) {
	      return null;
	    }
	    else {
	      Node<E> temp = head;
	      head = head.next;
	      size--;
	      if (head == null) {
	        tail = null;
	      }
	      return temp.element;
	    }
	  }

	  /** Remove the last node and
	   * return the object that is contained in the removed node. */
	  public E removeLast() {
	    if (size == 0) {
	      return null;
	    }
	    else if (size == 1) {
	      Node<E> temp = head;
	      head = tail = null;
	      size = 0;
	      return temp.element;
	    }
	    else {
	      Node<E> current = head;

	      for (int i = 0; i < size - 2; i++) {
	        current = current.next;
	      }

	      Node<E> temp = tail;
	      tail = current;
	      tail.next = null;
	      size--;
	      return temp.element;
	    }
	  }

	  @Override /** Remove the element at the specified position in this 
	   *  list. Return the element that was removed from the list. */
	  public E remove(int index) {   
	    if (index < 0 || index >= size) {
	      return null;
	    }
	    else if (index == 0) {
	      return removeFirst();
	    }
	    else if (index == size - 1) {
	      return removeLast();
	    }
	    else {
	      Node<E> previous = head;

	      for (int i = 1; i < index; i++) {
	        previous = previous.next;
	      }

	      Node<E> current = previous.next;
	      previous.next = current.next;
	      current.next.prev = previous; // For a two-way linked list
	      size--;
	      return current.element;
	    }
	  }

	  @Override /** Override toString() to return elements in the list */
	  public String toString() {
	    StringBuilder result = new StringBuilder("[");

	    Node<E> current = head;
	    for (int i = 0; i < size; i++) {
	      result.append(current.element);
	      current = current.next;
	      if (current != null) {
	        result.append(", "); // Separate two elements with a comma
	      }
	      else {
	        result.append("]"); // Insert the closing ] in the string
	      }
	    }

	    return result.toString();
	  }

	  @Override /** Clear the list */
	  public void clear() {
	    size = 0;
	    head = tail = null;
	    contains(null);
	  }

		@Override /** Return true if this list contains the element e */
		public boolean contains(Object e) {
			Node<E> current = head;
			if (size == 0)
				return false;
			while (current != null) {
				if (current.element.equals(e))
					return true;
				else
					current = current.next;
			}
			return false;
//			System.out.println("Implementation left as an exercise");
//			return true;
		}

		@Override /** Return the element at the specified index */
		public E get(int index) {
			Node<E> current = head;
			if (index < 0 || index > size - 1)
				return null;
			for (int i = 0; i < index; i++) {
				current = current.next;
			}
			return current.element;
//			System.out.println("Implementation left as an exercise");
//			return null;
		}

	  @Override /** Return the index of the head matching element in 
	   *  this list. Return -1 if no match. */
	  public int indexOf(Object e) {
		  Node<E> current = head;
		  for (int index = 0; current != null; index++,current = current.next) 
			  if (current.element.equals(e))
				return index;
		  
	 	  return -1;
//		  System.out.println("Implementation left as an exercise");
//		  return 0;
	    
	  }

	  @Override /** Return the index of the last matching element in 
	   *  this list. Return -1 if no match. */
	  public int lastIndexOf(Object e) {
		  Node<E> current = tail;
		  int lastFoundIndex = -1;
		  for (int index = size-1; current != head; index--,current = current.prev) 
			  if (current.element.equals(e))
				return index;
		  return lastFoundIndex;
//		  System.out.println("Implementation left as an exercise");
//		  return 0;
	  }

	  @Override /** Replace the element at the specified position 
	   *  in this list with the specified element. */
	  public E set(int index, E e) {
		  Node<E> current = head;
		  E oldValue;
		  if (index < 0 || index >= size)
			  return null;
		  for (int i = 0; i < index; i++) 
			  current = current.next;
		  oldValue = current.element;
		  current.element = e;
		  return oldValue;
//		  System.out.println("Implementation left as an exercise");
//		  return null;
  	  }

	  @Override /** Override iterator() defined in Iterable */
	  public ListIterator<E> listIterator() {
	    return new LinkedListIterator();
	  }

    @Override
    public ListIterator<E> listIterator(int index) {
        // TODO Auto-generated method stub
        return new LinkedListIterator(index);
    }

	  private void checkIndex(int index) {
	    if (index < 0 || index >= size)
	      throw new IndexOutOfBoundsException
	        ("Index: " + index + ", Size: " + size);
	  }
	  
	  private class LinkedListIterator<E> implements ListIterator<E> {
	      private Node<E> current = (Node<E>)head; // Current index
	      int index = 0;

	      public LinkedListIterator() {
	      }
	      
	      public LinkedListIterator(int index) {
	        if (index < 0 || index > size)
	          throw new IndexOutOfBoundsException("Index: " + index + ", Size: "
	            + size);
	        for (int nextIndex = 0; nextIndex < index; nextIndex++)
	          current = current.next;
	      }
	    
	      public boolean hasNext() {
	        return (current != null);
	      }

	      public E next() {
	        E e = current.element;
	        current = current.next;
	        return e;
	      }

	      public void remove() {
	        System.out.println("Not implemented");
	      }

	      public void add(E e) {
	        System.out.println("Not implemented");
	      }

	      public boolean hasPrevious() {
	        return current != head;
	      }

	      public int nextIndex() {
	        System.out.println("Not implemented");
	        return 0;
	      }

	      public E previous() {
	        E e = current.element;
	        current = current.prev;
	        return e;
	      }

	      public int previousIndex() {
	        System.out.println("Not implemented");
	        return 0;
	      }

	      @Override
	      public void set(E e) {
	        current.element = e; // TODO Auto-generated method stub
	      }
	    }
	  
	  private static class Node<E> {
	    E element;
	    Node<E> next;
	    Node<E> prev;

	    public Node(E element) {
	      if (element == null)
		    throw new NullPointerException();
	      this.element = element;
	    }
	  }
	
	

	@Override
	public int size() {
		return size;
	}

}
