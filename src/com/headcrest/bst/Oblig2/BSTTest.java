package com.headcrest.bst.Oblig2;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.*;


public class BSTTest {
	BST<Integer> tree;
	Integer[] intArr = {20,10,30,5,35};

	@Before
	public void setUp() {
		tree = new BST<>(intArr);
		
	}
	@Test
	public void getNode_getNodeWithElement35_ShouldReturnThatNode() {
		assertThat(tree.getNode(35).element,equalTo(35));
	}
	
	@Test
	public void getNode_notExistingElement_ShouldNotBeFound() {
		assertThat(tree.getNode(34),equalTo(null));
	}
	
	@Test
	public void isLeaf_5And35_ShouldBeLeafs() {
		assertThat(tree.isLeaf(5),is(true));
		assertThat(tree.isLeaf(35),is(true));
	}
	
	@Test
	public void isLeaf_20And10_ShouldNOTBeLeafs() {
		assertThat(tree.isLeaf(20),is(false));
		assertThat(tree.isLeaf(10),is(false));
	}
	
	@Test
	public void getPath_from35_ShouldBe35_30_20() {
		Integer[] iResult = {35,30,20};
		ArrayList<Integer> aResult = new ArrayList<Integer>(Arrays.asList(iResult));
		assertThat(tree.getPath(35),equalTo(aResult));
	}
	
	@Test
	public void preorderIterator_firstElement_ShouldBeRoot_thatIs20() {
		java.util.Iterator it = tree.iterator();
		assertThat(it.next(),equalTo(20));
	}
	
	@Test
	public void preorderIterator_removeRoot_ShouldReturnNewRoot_thatIs10() {
		java.util.Iterator it = tree.iterator();
		it.remove();
		assertThat(it.next(),equalTo(10));
	}
	
	@Test (expected = NullPointerException.class)
	public void insert_nullValue_ShouldThrowNullPointerException() {
		tree.insert(null);
	}

}

