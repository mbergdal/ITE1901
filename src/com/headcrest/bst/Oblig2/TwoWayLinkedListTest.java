package com.headcrest.bst.Oblig2;
import java.util.ListIterator;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Before;
import org.junit.Test;

public class TwoWayLinkedListTest {
    TwoWayLinkedList<String> list;
    String[] cities = {"Oslo", "Amsterdam", "London", "New York", "Harstad", "Narvik"};

    @Before
	public void setUp() throws Exception {
		list = new TwoWayLinkedList<>(cities);
	}

	@Test
	public void contains_LookupNarvik_ShouldReturnTrue() {
		assertThat(list.contains("Narvik"), is(true));
	}
	
	@Test
	public void contains_NotExistingCityLookup_ShouldReturnFalse() {
		assertThat(list.contains("DoesNotExist"), is(false));
	}
	
	@Test
	public void get_index0_ShouldReturnFirstElement() {
		String result = list.get(0);
		assertThat(result, equalTo("Oslo"));
	}
	
	@Test
	public void get_indexOfLast_ShouldReturnLastElement() {
		String result = list.get(5);
		assertThat(result, equalTo("Narvik"));
	}
	
	@Test
	public void indexOf_FirstLookup_ShouldBe_0() {
		assertThat(list.indexOf("Narvik"), equalTo(5));
	}
	
	@Test
	public void indexOf_LastLookup_ShouldBe_5() {
		assertThat(list.indexOf("Oslo"), equalTo(0));
	}
	
	@Test
	public void indexOf_NotExisting_ShouldBe_Minus1() {
		assertThat(list.indexOf("Stavanger"), equalTo(-1));
	}
	
	@Test
	public void lastIndexOf_Oslo_ShouldBe_5() {
		assertThat(list.lastIndexOf("Narvik"), equalTo(5));
	}
	
	@Test
	public void lastIndexOf_London_ShouldBe_4() {
		list.add("London");
		assertThat(list.lastIndexOf("London"), equalTo(6));
	}
	
	@Test
	public void lastIndexOf_NotExisting_ShouldBe_minus1() {
		assertThat(list.lastIndexOf("Stavanger"), equalTo(-1));
	}
	
	@Test
	public void set_NewValue_ShouldFindNewValueAtCorrectIndex() {
		String result = list.set(list.indexOf("Amsterdam"),"Liverpool");
		assertThat(result, equalTo("Amsterdam"));
		assertThat(list.indexOf("Liverpool"), equalTo(1));
	}
	
	@Test
	public void listIterator_withIndex_index2ShouldReturn_London() {
		ListIterator<String> iter = list.listIterator(2);
		String result = iter.next();
		assertThat(result, equalTo("London"));
	}
}
